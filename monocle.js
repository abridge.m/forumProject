// ==UserScript==
// @name        Monocle
// @namespace   Glibertarians
// @include     *glibertarians.com/2*
// @version     1
// @grant       none
// ==/UserScript==


//Feature Variables
var expandMainCol = true;
var chgTimeZone = true;
var footerToolbar = true;
var fullCommentReply = false;
var compressView = true;
//make choice persistent for hide/show old threads
var hideThreadsStorageTag = "hideOldThreads";
if ( typeof(localStorage[hideThreadsStorageTag]) == 'undefined' ) {
    localStorage[hideThreadsStorageTag] = 0;
}

//add stylesheet in one place vs everywhere
//called from createFooter, since that is only done once
function doStyleSheet(){
    var styleEle = document.createElement('style');
    styleEle.type = 'text/css';
    styleEle.id = "monocle-style-css";
    styleEle.textContent = '#monocle_tb_container { position: fixed; z-index: 100; left: 0px; bottom: 0px; height: 60px; width: 100%; } ' +
        '#monocle_tb_container.mini { height: 30px; } ' +
        '#monocle_toolbar { left:30px; bottom:0px; height: 30px; width:100%; background-color: rgba(0,0,0,0.5); } ' +
        '#monocle_tb_container.mini #monocle_toolbar { height: 0px; } ' +
        'input.mono_button { background-image: none; padding: 1px 1px 1px 1px; height: 30px; text-shadow: #ffffff 0px 0px 2px; -webkit-font-smoothing: antialiased } ' +
        '#unread_button { float:left; width: 160px; background-color: rgba(0,0,0,0.9); color: white; } ' +
        '#old_thread_button { float:left; width: 160px; background-color: rgba(0,0,0,0.9); color: white } ' +
        '#toggle_button { left:0px; bottom:0px; width:20px; background-color: rgba(0,0,0,0.5); }' +
        '#comment-wrap.onlyNew li.comment.no-new-comment { display: none; } ';
    if ( compressView ) {
        styleEle.textContent = styleEle.textContent + ' article.comment-body { margin-bottom: 0px; } ';
    }
    styleEle.textContent = styleEle.textContent + '';
    document.head.appendChild(styleEle);
}

//add new class when there is no new comments below, making the toggling a matter of changing the class of the parent object
function parseComments(){
  var commentList = document.querySelector(".commentlist");
  var comments = commentList.querySelectorAll("li");
  for(var i = 0; i < comments.length; i++){
    if ( comments[i].getElementsByClassName("new-comment").length == 0 && !comments[i].classList.contains("new-comment")) {
        comments[i].classList.add("no-new-comment");
    }
  }
}

//function to remove the sidebar
function expandMainColumn(){
  var sideCol = document.querySelector(".et_pb_extra_column_sidebar");
  sideCol.parentNode.removeChild(sideCol);
  var mainCol = document.querySelector(".et_pb_extra_column_main");
  mainCol.className = "";
}

//functions to set the local time zone on all comments
function setTimeZone(){
  var comments = document.querySelectorAll(".comment_date");
  for (var i = 0; i < comments.length; i++){
    adjustTime(comments[i]);
  }
}

function adjustTime(commentDate){ 
  var dateTokens = commentDate.innerHTML.split(" ");
  var dateObj = new Date(dateTokens[1] + " " + dateTokens[2] + " " + dateTokens[3] + " " + dateTokens[5] + " " + dateTokens[6] + " GMT-0600");
  var options = {
    year: "numeric", month: "long",
    day: "numeric", hour: "2-digit", minute: "2-digit"
  };
  commentDate.innerHTML = "on " + dateObj.toLocaleTimeString("en-us",options);
}

//create footer toolbar
var unreadCount = 0;
var oldThreadVisible = true;
function createFooter(){
  var toolbar = document.createElement("div");
  toolbar.id = "monocle_tb_container";

  //visible portion
  var activebar = document.createElement("div");
  activebar.id = "monocle_toolbar";
  activebar.className = "visible";
  //scrolls to unread comments
  var nextUnreadButton = document.createElement("input");
  nextUnreadButton.id = "unread_button";
  nextUnreadButton.type = "button";
  nextUnreadButton.className = "mono_button";
  nextUnreadButton.value = document.querySelectorAll(".new-comment").length + " Unread Comments";
  if (document.querySelectorAll(".new-comment").length == 1){
    nextUnreadButton.value = document.querySelectorAll(".new-comment").length + " Unread Comment";
  }
  nextUnreadButton.addEventListener("click",seekNextUnread);
  activebar.appendChild(nextUnreadButton);
  //toggles visibility of old threads
  var oldThreadButton = document.createElement("input");
  oldThreadButton.id = "old_thread_button";
  oldThreadButton.type = "button";
  oldThreadButton.className = "mono_button";
  oldThreadButton.value="Hide Old Threads";
  oldThreadButton.addEventListener("click", toggleOldThreads);
  activebar.appendChild(oldThreadButton);
  //toggles visibility of toolbar
  var toggleButton = document.createElement("input");
  toggleButton.id = "toggle_button";
  toggleButton.type = "button";
  toggleButton.className = "mono_button";
  toggleButton.value = "v";
  toggleButton.addEventListener("click",toggleToolbar);
  toolbar.appendChild(toggleButton);
  toolbar.appendChild(activebar);
  document.querySelector(".comment-respond").appendChild(toolbar);
  setShowThreads(oldThreadButton);
  doStyleSheet();
  parseComments();
}

function toggleOldThreads(){
  localStorage[hideThreadsStorageTag] = (localStorage[hideThreadsStorageTag] == 0) ? 1 : 0;
  setShowThreads(this);
}

function setShowThreads(thisEle) {
  if (localStorage[hideThreadsStorageTag] == 1){
    thisEle.value = "Show Old Threads";
    document.getElementById("comment-wrap").classList.add("onlyNew");
  } 
  else{
    thisEle.value = "Hide Old Threads";
    document.getElementById("comment-wrap").classList.remove("onlyNew");
  }
}

function seekNextUnread(){
  var unreadList = document.querySelectorAll(".new-comment");
  if (unreadCount >= unreadList.length){
    unreadCount = 0;
  }
  if (unreadList.length > 0){
   unreadList[unreadCount].scrollIntoView();
   window.scrollBy(0,-50);
  }
  unreadCount+=1;
}

function toggleToolbar(){
  if ( document.getElementById("monocle_tb_container").classList.contains("mini") ) {
    document.getElementById("monocle_tb_container").classList.remove("mini");
    document.getElementById("toggle_button").value = "v";
  }
  else{
    document.getElementById("monocle_tb_container").classList.add("mini");
    document.getElementById("toggle_button").value = "^";
  }
}

//collapse the reply button into the comment
function collapseReply(){
  var comments = document.querySelectorAll(".comment");
  for (var i = 0; i < comments.length; i++){    
    var replyButton = comments[i].querySelector(".reply-container");
    var replyLink = replyButton.querySelector("a");
    //replyButton.parentNode.removeChild(replyButton);    
  }
}

function replyToComment(){
   var replyLink = this.querySelector("a");   
}

//Main

if (expandMainCol){
  try{
    expandMainColumn();
  }
  catch(error){}
}


if (chgTimeZone){
  try{
    setTimeZone();
  }
  catch(error){}
}

if (footerToolbar){
  try{
    createFooter();
  }
  catch(error){}
}

if (fullCommentReply){
  try{
    collapseReply();
  }
  catch(error){}
}
